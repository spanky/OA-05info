var replace = require("replace");

// use:
let exChangeMap = [
  ['new.05oa.com', 'xcx.jyxflygl.com'],
  ['wxa596d4c2ef0dff2e', 'wx4f7e30771b49b74e']
];
exChangeMap.forEach(item => {
  replace({
    regex: item[0],
    replacement: [item[1]],
    paths: ['./unpackage/dist/build/mp-weixin/'],
    recursive: true,
    silent: true,
});
})

