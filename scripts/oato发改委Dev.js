var replace = require("replace");

// use:
let exChangeMap = [
  ['new.05oa.com', 'xcx.jyxflygl.com'],
  ['wxa596d4c2ef0dff2e', 'wx4f7e30771b49b74e']
];
exChangeMap.forEach(item => {
  replace({
    regex: item[0],
    replacement: [item[1]],
    paths: ['./main.js', './pages/', './utils/', './styles/', './manifest.json', './pages.json'],
    recursive: true,
    silent: true,
});
})

