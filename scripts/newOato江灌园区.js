var replace = require("replace");

// use:
let exChangeMap = [
  ['xcx.05data.cn', 'jgzhyq.05info.cn'],
  ['wxbefbb1d7813f5ff6', 'wxdb4606e5c5639ab1']
];
exChangeMap.forEach(item => {
    replace({
        regex: item[0],
        replacement: [item[1]],
        paths: ['./unpackage/dist/build/mp-weixin/'],
        recursive: true,
        silent: true,
    });
})

exChangeMap.forEach(item => {
    replace({
        regex: item[0],
        replacement: [item[1]],
        paths: ['./unpackage/dist/dev/mp-weixin/'],
        // paths: ['./main.js', './pages/', './utils/', './styles/', './manifest.json', './pages.json'],
        recursive: true,
        silent: true,
    });
})
