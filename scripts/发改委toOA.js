var replace = require("replace");

// use:
let exChangeMap = [
  ['xcx.jyxflygl.com', 'new.05oa.com'],
  ['wx4f7e30771b49b74e', 'wxa596d4c2ef0dff2e']
];
exChangeMap.forEach(item => {
  replace({
    regex: item[0],
    replacement: [item[1]],
    paths: ['./main.js', './pages/', './utils/', './styles/', './manifest.json', './pages.json'],
    recursive: true,
    silent: true,
});
})

