var replace = require("replace");

// use:
let exChangeMap = [
  ['xcx.05data.cn', 'xcx.jywsljt.com'],
  ['wxbefbb1d7813f5ff6', 'wx1178e4a47a6562a0']
];
exChangeMap.forEach(item => {
    replace({
        regex: item[0],
        replacement: [item[1]],
        paths: ['./unpackage/dist/build/mp-weixin/'],
        recursive: true,
        silent: true,
    });
})

exChangeMap.forEach(item => {
    replace({
        regex: item[0],
        replacement: [item[1]],
        paths: ['./unpackage/dist/dev/mp-weixin/'],
        recursive: true,
        silent: true,
    });
})

