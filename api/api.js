module.exports = {
	$APIMAP: {
		'获取空间配置': '/static/json/getAppConfigs.json',
		'获取文章列表': '/mobile/wx/coloum/getList.jsp',
		'获取聊天室人员': '/mobile/mpoa/qq/getChatUsers.jsp',
	},

	$APIDATA: {
        // 'wx/weChatCode.jsp?code=': {"session_key":"lCWgi20FoOwKoZoB70WVKA==","openid":"o52VT5A7Ojgu3A6drKvXchyqtKQ0","success":true,"msg":""},
        // 'isExsit.jsp?openId=': {"code":1,"msg":"","data":{"exsit":true,"userInfo":{userId: 123456,age: 32,"name":"周刚","credentialsNo":"320281199606286774",post: "部门经理",dept: "开发二部","phone":"18795905631","openId":"o52VT5A7Ojgu3A6drKvXchyqtKQ0","faceValidate":"","rz":false,"avatar":"https://thirdwx.qlogo.cn/mmopen/vi_32/DYAIOgq83eqb42zqwmLtCYe8OAqAW1jIGb6icEKRtGBgQEVSfKZicyfMIS4zSMzol3ynKWlWlofEibAo8bia8haFOw/132","nickName":"Spanky"}},"success":false},
        'getAppConfigs.json': {
			"code": 0,
			"data": {
				"spaceName": "协同办公系统",
				"topAd": [{
					"image": "/wxAssets/wxImages/menuIcon/banner.png"
				}],
				"commonMenus": [{
					"name": "待办工作",
					"icon": "/wxAssets/wxImages/menuIcon/dbgz.png",
					"style": "",
					url: '/pages/content/list?loadJSON=/mobile/mpoa/待办工作/config.json'
				}, {
					"name": "待收getModules.jsp",
					"icon": "/wxAssets/wxImages/menuIcon/dsgw.png",
					"style": ""
				}, {
					"name": "公告通知",
					"icon": "/wxAssets/wxImages/menuIcon/ggtz.png",
					"style": ""
				}, {
					"name": "工作动态",
					"icon": "/wxAssets/wxImages/menuIcon/gzdt.png",
					"style": ""
				}, {
					"name": "动向签到",
					"icon": "/wxAssets/wxImages/menuIcon/dxqd.png",
					"style": "",
					url: '/pages/qiandao/index'
				}],
				"extMenus": [{
					"groupName": "功能应用",
					"list": [{
						"name": "工作任务",
						"icon": "/wxAssets/wxImages/menuIcon/gzrw.png",
						"style": "",
						h5: '/hybrids/tasks/index.html?openId=【openId】'
					}, {
						"name": "公文管理",
						"icon": "/wxAssets/wxImages/menuIcon/gwgl.png",
						"style": ""
					}, {
						"name": "会议管理",
						"icon": "/wxAssets/wxImages/menuIcon/hygl.png",
						"style": ""
					}, {
						"name": "日程安排",
						"icon": "/wxAssets/wxImages/menuIcon/rcap.png",
						"style": "",
						url:'/pages/richeng/index'
					}, {
						"name": "请假管理",
						"icon": "/wxAssets/wxImages/menuIcon/qjgl.png",
						"style": ""
					}, {
						"name": "报销管理",
						"icon": "/wxAssets/wxImages/menuIcon/bxgl.png",
						"style": ""
					}, {
						"name": "出行/出差管理",
						"icon": "/wxAssets/wxImages/menuIcon/cxgl.png",
						"style": "",
						url:'/pages/travel/shouy'
					}, {
						"name": "用车管理",
						"icon": "/wxAssets/wxImages/menuIcon/ycgl.png",
						"style": ""
						
					}]
				}],
				"contents": [{
					"columnName": "单位动态"
				}, {
					"columnName": "党建咨询"
				}],
				"menuButtons": [{
					"name": "发文申请",
					"color": "#f9b445",
					"icon": "/wxAssets/wxImages/menuIcon/dbgz.png"
				}, {
					"name": "请假申请",
					"color": "#f57385",
					"icon": "/wxAssets/wxImages/menuIcon/dsgw.png"
				}, {
					"name": "日程安排",
					"color": "#69e5d0",
					"icon": "/wxAssets/wxImages/menuIcon/ggtz.png",
				}, {
					"name": "报销申请",
					"color": "#50d4e3",
					"icon": "/wxAssets/wxImages/menuIcon/gzdt.png"
				}, {
					"name": "出行/出差申请",
					"color": "#5fb1fe",
					"icon": "/wxAssets/wxImages/menuIcon/dxqd.png"
				}, {
					"name": "用车申请",
					"color": "#69e5d0",
					"icon": "/wxAssets/wxImages/menuIcon/dbgz.png"
				}, {
					"name": "任务创建",
					"color": "#f9b445",
					"icon": "/wxAssets/wxImages/menuIcon/dsgw.png",
					h5: '/hybrids/tasks/form.html?openId=【openId】'
				}, {
					"name": "议题/会议室申请",
					"color": "#5fb1fe",
					"icon": "/wxAssets/wxImages/menuIcon/gzdt.png"
				}]
			}
		
		},
		'/static/json/getContentList.json': {
			"code": 0,
			"data": [{
				"name": "江阴召开深入推进更高水平平安江阴建设动员部署会",
				"date": "2022-03-15 14:40:06",
				"pic": "/static/test.png"
			}, {
				"name": "深入推进更高水平平安江阴建设动员部署会深入推进更高水平平安江阴建设动员部署会",
				"date": "2022-03-15 14:40:06",
				"pic": "/static/logo.png"
			}, {
				"name": "阴建设动员部署会阴建设动员部署会阴建设动员部署会阴建设动员部署会",
				"date": "2022-03-15 14:40:06",
				"pic": "/static/test.png"
			}, {
				"name": "平平安江阴建设动员部平平安江阴建设动员部平平安江阴建设动员部",
				"date": "2022-03-15 14:40:06",
				"pic": "/static/logo.png"
			}, {
				"name": "水平平安江阴建水平平安江阴建水平平安江阴建水平平安江阴建",
				"date": "2022-03-15 14:40:06",
				"pic": "/static/test.png"
			}, {
				"name": "江阴召开深入推进更高水平平安江阴建设动员部署会",
				"date": "2022-03-15 14:40:06",
				"pic": "/static/test.png"
			}, {
				"name": "深入推进更高水平平安江阴建设动员部署会深入推进更高水平平安江阴建设动员部署会",
				"date": "2022-03-15 14:40:06",
				"pic": "/static/logo.png"
			}, {
				"name": "阴建设动员部署会阴建设动员部署会阴建设动员部署会阴建设动员部署会",
				"date": "2022-03-15 14:40:06",
				"pic": "/static/test.png"
			}, {
				"name": "平平安江阴建设动员部平平安江阴建设动员部平平安江阴建设动员部",
				"date": "2022-03-15 14:40:06",
				"pic": "/static/logo.png"
			}, {
				"name": "水平平安江阴建水平平安江阴建水平平安江阴建水平平安江阴建",
				"date": "2022-03-15 14:40:06",
				"pic": "/static/test.png"
			}, {
				"name": "江阴召开深入推进更高水平平安江阴建设动员部署会",
				"date": "2022-03-15 14:40:06",
				"pic": "/static/test.png"
			}, {
				"name": "深入推进更高水平平安江阴建设动员部署会深入推进更高水平平安江阴建设动员部署会",
				"date": "2022-03-15 14:40:06",
				"pic": "/static/logo.png"
			}, {
				"name": "阴建设动员部署会阴建设动员部署会阴建设动员部署会阴建设动员部署会",
				"date": "2022-03-15 14:40:06",
				"pic": "/static/test.png"
			}, {
				"name": "平平安江阴建设动员部平平安江阴建设动员部平平安江阴建设动员部",
				"date": "2022-03-15 14:40:06",
				"pic": "/static/logo.png"
			}, {
				"name": "水平平安江阴建水平平安江阴建水平平安江阴建水平平安江阴建",
				"date": "2022-03-15 14:40:06",
				"pic": "/static/test.png"
			}]
		},		
		
		'/static/json/getContentList.json?columnName=单位动态': {
			"code": 0,
			"data": [{
				"name": "深入推进更高水平平安江阴建设动员部署会深入推进更高水平平安江阴建设动员部署会",
				"date": "2022-03-15 14:40:06",
				"pic": "/static/logo.png"
			}, {
				"name": "阴建设动员部署会阴建设动员部署会阴建设动员部署会阴建设动员部署会",
				"date": "2022-03-15 14:40:06",
				"pic": "/static/test.png"
			}, {
				"name": "平平安江阴建设动员部平平安江阴建设动员部平平安江阴建设动员部",
				"date": "2022-03-15 14:40:06",
				"pic": "/static/logo.png"
			}, {
				"name": "水平平安江阴建水平平安江阴建水平平安江阴建水平平安江阴建",
				"date": "2022-03-15 14:40:06",
				"pic": "/static/test.png"
			}, {
				"name": "江阴召开深入推进更高水平平安江阴建设动员部署会",
				"date": "2022-03-15 14:40:06",
				"pic": "/static/test.png"
			}, {
				"name": "深入推进更高水平平安江阴建设动员部署会深入推进更高水平平安江阴建设动员部署会",
				"date": "2022-03-15 14:40:06",
				"pic": "/static/logo.png"
			}, {
				"name": "阴建设动员部署会阴建设动员部署会阴建设动员部署会阴建设动员部署会",
				"date": "2022-03-15 14:40:06",
				"pic": "/static/test.png"
			}, {
				"name": "平平安江阴建设动员部平平安江阴建设动员部平平安江阴建设动员部",
				"date": "2022-03-15 14:40:06",
				"pic": "/static/logo.png"
			}, {
				"name": "水平平安江阴建水平平安江阴建水平平安江阴建水平平安江阴建",
				"date": "2022-03-15 14:40:06",
				"pic": "/static/test.png"
			}, {
				"name": "江阴召开深入推进更高水平平安江阴建设动员部署会",
				"date": "2022-03-15 14:40:06",
				"pic": "/static/test.png"
			}, {
				"name": "深入推进更高水平平安江阴建设动员部署会深入推进更高水平平安江阴建设动员部署会",
				"date": "2022-03-15 14:40:06",
				"pic": "/static/logo.png"
			}, {
				"name": "阴建设动员部署会阴建设动员部署会阴建设动员部署会阴建设动员部署会",
				"date": "2022-03-15 14:40:06",
				"pic": "/static/test.png"
			}, {
				"name": "平平安江阴建设动员部平平安江阴建设动员部平平安江阴建设动员部",
				"date": "2022-03-15 14:40:06",
				"pic": "/static/logo.png"
			}, {
				"name": "水平平安江阴建水平平安江阴建水平平安江阴建水平平安江阴建",
				"date": "2022-03-15 14:40:06",
				"pic": "/static/test.png"
			}]
		},
        
        '/mobile/oa/getAllConcats.jsp' : {
            code: 0, 
            data: [{
                id: 0,
                label: '领悟信息',
                children: [
                    { id: 1, label: '严洪涛', icon: 'https://lyggntest.05oa.com:8288/web/wggl/202204/nn20a17e4gg/LkFkrGUFJxbF1a181fb35ec3bcfe5039d7474eeb1c89.jpg', sex: '男', post: 'CEO', tel: '13338777813', mail: 'yht@05info.com', dept: '管理组' },
                    { id: 2, label: '总经办' , children: [
                        { id: 3, label: '王林熙', icon: '' },
                        { id: 4, label: '卢佳丽', icon: '' },
                        { id: 5, label: '刘佳', icon: '' },
                        { id: 6, label: '孙晨龙', icon: '' },
                        { id: 7, label: '胡斐', icon: '' },
                        { id: 8, label: '赵洁静', icon: '' }
                    ]},
                    { id: 9, label: '江阴总公司' , children: [
                        { id: 10, label: '张军', icon: '' },
                        { id: 11, label: '吴钡', icon: '' },
                        { id: 12, label: '肖娟', icon: '' },
                        { id: 13, label: '开发一部' , children: [
                            {
                                    id: 123456, label: '仰一鸣', sex: '男', post: '部门经理', tel: '18795905631', mail: '18795905631@163.com', dept: '开发一部',
                                    icon: 'https://thirdwx.qlogo.cn/mmopen/vi_32/DYAIOgq83eqb42zqwmLtCYe8OAqAW1jIGb6icEKRtGBgQEVSfKZicyfMIS4zSMzol3ynKWlWlofEibAo8bia8haFOw/132'
                            },
                            { id: 15, label: '刘一彬', icon: '' },
                            { id: 16, label: '黄宇峰', icon: '' },
                            { id: 17, label: '顾越', icon: '' },
                            { id: 18, label: '李澄威', icon: '' },
                        ]},
                        { id: 19, label: '开发二部' , children: [
                            { id: 20, label: '周刚', icon: '' },
                            { id: 21, label: '黄梦娇', icon: '' },
                            { id: 22, label: '李凡', icon: '' },
                            { id: 23, label: '陆鸣一', icon: '' },
                            { id: 24, label: '陈林湖', icon: '' },
                        ]},
                        { id: 25, label: '测试部' , children: [
                            { id: 26, label: '陈钰', icon: '' },
                            { id: 27, label: '刘俊辉', icon: '' },
                        ]}
                    ]},							
                ]
            }]
        },
		'/mobile/mpoa/qq/getChatUsers.jsp': {
			code: 0,
			data: [
				{ id: 10, label: '张军', icon: '' },
				{
					id: 123456, label: '仰一鸣', sex: '男', post: '部门经理', tel: '18795905631', mail: '18795905631@163.com', dept: '开发一部',
					icon: 'https://thirdwx.qlogo.cn/mmopen/vi_32/DYAIOgq83eqb42zqwmLtCYe8OAqAW1jIGb6icEKRtGBgQEVSfKZicyfMIS4zSMzol3ynKWlWlofEibAo8bia8haFOw/132'
				},
				{ id: 15, label: '刘一彬', icon: '' },
				{ id: 16, label: '黄宇峰', icon: '' },
				{ id: 17, label: '顾越', icon: '' },
				{ id: 18, label: '李澄威', icon: '' },
			]
		}

	}
}
