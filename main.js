import App from './App'
import uView from '@/uni_modules/uview-ui'
import Vue from 'vue'
import store from './store/vuex.js'

import {isH5} from '@/utils/Platform.js'
import authoJudge from '@/utils/authoJudge.js';
import CommonUtil from '@/utils/commonUtil.js';
import globalMixins from '@/utils/globalMixins.js';

Array.prototype.last = function () {
    return this.slice(-1)[0]
}

Vue.use(uView)
Vue.config.productionTip = false
App.mpType = 'app'
Vue.mixin(globalMixins)

var API_PREFIX = 'https://xcx.05data.cn';
// API_PREFIX = 'http://localhost:8080'
Vue.prototype.API_PREFIX = API_PREFIX;

Vue.prototype.$Util = CommonUtil;
Vue.prototype.$jg = authoJudge;

const app = new Vue({
    ...App,
		store
})
global.app = app;

app.$mount()