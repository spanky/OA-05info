import Vue from 'vue'
import Vuex from 'vuex'
Vue.use(Vuex)
const store = new Vuex.Store({
    state: {
			currentTabIndex: 0,
			globalConfig: '',
			userInfo: '',
			hasLogin: false,
			userPosition: '',
			lnglat: '',
			menuConfigs: ''
		},
    mutations: {
			changeTab (state, provider) {
				state.currentTabIndex = provider;
			},
			doLogin (state, provider) {
				state.userInfo = provider;
				uni.setStorageSync('userInfo', provider);
				state.hasLogin = true;
			},
			setGlobalConfig(state, provider) {
				state.globalConfig = provider;
			},
			doLogout (state, provider) {
				state.userInfo = '';
				uni.setStorageSync('userInfo', '');
				state.hasLogin = false;
			},
			setPosition(state, provider) {
				state.userPosition = provider;
				state.lnglat = [provider.longitude, provider.latitude]
			},
			setMenuConfigs (state, provider) {
				state.menuConfigs = provider;
			},
		},
    actions: {}
})
export default store