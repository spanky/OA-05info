module.exports = {
	//#ifdef APP
	isH5: false,
	isApp: true,
	isMp: false,
	//#endif
	//#ifdef H5
	isH5: true,
	isApp: false,
	isMp: false,
	//#endif
	//#ifdef MP
	isH5: false,
	isApp: false,
	isMp: true,
	//#endif
	isDevelopment: process.env.NODE_ENV == 'development',
	isProduction: process.env.NODE_ENV == 'production'
};
