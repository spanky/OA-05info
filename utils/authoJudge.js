import store from '@/store/vuex.js'
import qs from 'qs';
import Vue from 'vue'
export default {
	judge: function (type = 'login') {
		var vm = this;
		return new Promise((resove, reject) => {
			if (type == 'login') {
				if (store.state.hasLogin) {
					resove();
				} else {
					var userInfo = uni.getStorageSync('userInfo');
					if (userInfo) {
						store.commit('doLogin', userInfo);
						resove();
					} else {
						global.app.doWxLogin().then(res => {
							global.app.getAndLoad('登录', '/mobile/wx/newApi/login.jsp?openId=' + res.openId, function(res2) {
							// vm.getAndLoad('登录', '/mobile/newApi/wx/isExsit.jsp?openId=' + res.openId, function(res2) {
								if (res2.success && res2.data) {
									store.commit('doLogin', res2.data);
									resove();
								} else {
									// 未登录
									var route = getCurrentPages()[0].route;
									var options = getCurrentPages()[0].options;
									var qsStr = qs.stringify(options);
									uni.setStorageSync('redirectPage', '/' + route + (qsStr ? ('?' + qsStr) : ''));
									uni.redirectTo({url: '/pages/login/index'})
									reject();
								}
							});
						})
					}
				}
			}
		});
		
	},
	getTabIndex: function (url) {
		return ["/pages/index/index", "/pages/qq/index", "/pages/content/index", "/pages/mine/index"].findIndex(n => {
			return url.includes(n.replace(/^pages/, '/pages'));
		});
	},
	isTab: function (url) {
		return this.getTabIndex(url) != -1;
	},
	jumpAfterLogin: function () {
		var redirectPage = uni.getStorageSync('redirectPage');
		if (redirectPage) {
			uni.setStorageSync('redirectPage', '');
			var tabIndex = this.getTabIndex(redirectPage);
			if (tabIndex != -1) {
				store.commit('changeTab', tabIndex);
				uni.switchTab({ url: redirectPage });
			} else {
				uni.redirectTo({ url: redirectPage });
			}
		} else {
			uni.switchTab({ url: '/pages/index/index' });
		}
	}
};
