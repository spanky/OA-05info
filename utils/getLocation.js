export default {
	methods: {
		__getAuthorize() {
			return new Promise((resolve, reject) => {
				uni.authorize({
					scope: 'scope.userLocation',
					success: () => {
						resolve()
					},
					fail: () => {
						reject()
					},
				})
			})
		},
		// 用户首次拒绝授权后(考虑是误点击)，弹框提示是否手动打开位置授权
		__openConfirm() {
			return new Promise((resolve, reject) => {
				uni.showModal({
					title: '请求授权当前位置',
					content: '需要获取您的当前位置信息',
					success: res => {
						if (res.confirm) {
							uni.openSetting().then(res => {
								if (res[1].authSetting['scope.userLocation'] === true) {
									resolve()
								} else {
									reject()
								}
							})
						} else if (res.cancel) {
							reject()
						}
					},
				})
			})
		},
		// 彻底拒绝位置获取
		__rejectGetLocation() {
			uni.showToast({
				title: '你拒绝了位置授权',
				icon: 'none',
				duration: 2000,
			})
		},
		// 确认授权后，获取用户位置
		__getLocationInfo() {
			const that = this
			return new Promise(resolve => {
				uni.getLocation({
					type: 'gcj02',
					wgs84: false,
					sHighAccuracy: false,
					highAccuracyExpireTime: 3000,
					success: function(res) {
						resolve(res)
					},
				})
			});
		},

		getUserLocation() {
			return new Promise(resolve => {
				this.__getAuthorize()
					.then(() => {
						this.__getLocationInfo().then(res => {
							resolve(res)
						})
					})
					.catch(() => {
						//   不同意给出弹框，再次确认
						this.__openConfirm().then(() => {
								this.__getLocationInfo().then(res => {
									resolve(res)
								})
							})
							.catch(() => {
								this.__rejectGetLocation()
							})
					})
			})
		}
	}
}
